package com.tucker.jenkins.search;

import com.tucker.jenkins.sort.strategypattern.SortContext;
import com.tucker.jenkins.sort.strategypattern.SortStrategy;

import java.util.Random;

/**
 * Created by beezuz on 1/1/16.
 */
public abstract class Search {

    public int[] pool = new int[50];
    Random random = new Random();
    SortContext sortContext;

    public Search() {}

    public Search(SortStrategy strategy) {
        this.sortContext = new SortContext(strategy);
        setSortStrategy(strategy);
        fillPool();
        sortPool(pool);
        System.out.println("Debug from Abstract Super Class");
    }

    public abstract int doSearch(int[] collection, int guess);

    public void setSortStrategy(SortStrategy strategy){
        this.sortContext.changeStrategy(strategy);
    }

    private void fillPool() {
        for (int i = 0; i < pool.length; i++) {
            int max = pool.length - 1;
            int min = 0;
            pool[i] = random.nextInt(max - min + 1) + 1;
        }
    }

    private void sortPool(int[] pool) {
        sortContext.sort(pool);
    }
}
