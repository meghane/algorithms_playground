package com.tucker.jenkins.search;

/**
 * Created by beezuz on 11/1/15.
 */
import com.tucker.jenkins.sort.strategypattern.SortStrategy;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;

public class BinarySearch extends Search {
    public static final Logger LOGGER = LogManager.getLogger(BinarySearch.class.getName());
    /**
     *  Pseudocode:
     *  Let minArrayPos = 0 and maxArrayPos = n-1 of collection
     *  Guess the average of maxArrayPos and minArrayPos, rounded down (so that it is an integer). (middle of collection)
     *  If you guessed the number, stop. You found it!
     *  If the guess was too low, that is, array[guess] < target, then set min = guess + 1.
     *  If the guess was too high, tha is, array[guess] > target, set max = guess - 1.
     *  Go back to step 2.
     */

   public BinarySearch(SortStrategy strategy) {
        super(strategy);
    }

    /**
     * @param collection
     * @param target
     * @return index of array the target value is at
     */
    public int doSearch(int[] collection, int target) {
        LOGGER.info("BinarySearch doSearch called for target {}", target);

        int minArrayPos = 0;
        int maxArrayPos = collection.length - 1;
        int guessArrayPos;

        while(minArrayPos <= maxArrayPos) {
            guessArrayPos = (minArrayPos + maxArrayPos)/2;

            if (target == collection[guessArrayPos]) {
                LOGGER.info("Match! Target {} is located at index {}", target, guessArrayPos);
                return guessArrayPos;
            }

            if (target > collection[guessArrayPos]) {
                minArrayPos = guessArrayPos + 1;
            }

            if (target < collection[guessArrayPos]) {
                maxArrayPos = guessArrayPos - 1;
            }
        }
        LOGGER.info("target {} not present in array ", target);
        return -1;
    }

}
