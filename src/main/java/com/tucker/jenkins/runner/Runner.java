package com.tucker.jenkins.runner;

import com.tucker.jenkins.search.BinarySearch;
import com.tucker.jenkins.sort.*;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * Created by beezuz on 12/12/15.
 */
public class Runner {
    public static final Logger LOGGER = LogManager.getLogger(Runner.class.getName());

    public static void main(String[] args) {
       LOGGER.info("Runner Class Executing . . .");
        BinarySearch binarySearch = new BinarySearch(new SelectionSort());
        int target = 27;
        int result = binarySearch.doSearch(binarySearch.pool, target);

    }
}


