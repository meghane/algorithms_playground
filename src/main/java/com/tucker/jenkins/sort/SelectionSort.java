package com.tucker.jenkins.sort;

import com.tucker.jenkins.sort.strategypattern.SortStrategy;

/**
 * Created by beezuz on 12/12/15.
 */
public class SelectionSort extends SortStrategy {
    /**
     * Find smallest value, swap it with 1st index in collection
     * Find second-smallest value, swap with 2nd index in collection
     * Continue until collection is sorted
     */

    @Override
    public void sort(int[] collection) {

        for(int i = 0; i < collection.length; i++) {
            int minIndex = indexOfMinimum(collection, i);
            swap(collection, minIndex, i);
        }
    }
    /**
     *  Find index of minimum value in subarray
     */
    private int indexOfMinimum(int[] collection, int startIndex) {
        int minValue = collection[startIndex];
        int minIndex = startIndex;

        /**
         * Loop through items starting with minIndex/startIndex + 1
         * Find the index where the lowest value is
         */
        for(int i = minIndex + 1; i < collection.length; i++) {
            if(collection[i] < minValue) {
                minIndex = i;
                minValue = collection[i];
            }
        }
        return minIndex;
    }

    private void swap(int[] collection, int index1, int index2) {
        int tempValueHolder = collection[index1];
        collection[index1] = collection[index2];
        collection[index2] = tempValueHolder;
    }

}
