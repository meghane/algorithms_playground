package com.tucker.jenkins.sort;


import com.tucker.jenkins.sort.strategypattern.SortStrategy;

import java.util.Arrays;

/**
 * Created by beezus on 10/11/16.
 */
public class MergeSort extends SortStrategy {

    private int[] collection;

    /**
     * n log n
     * Split array into two equal pieces, recursively sorts, and
     * then merges subarrays back into original array
     * @param collection
     */
    @Override
    public void sort(int[] collection) {

        this.collection = collection;
        splitArray(0, collection.length - 1);
    }

    public void splitArray(int beginning, int end) {

        /**
         * Base case array size is 0 or 1
         */
        if (beginning < end) {

            /**
             * Calculate middle of array
             */
            int middle1 = (beginning + end) / 2;
            int middle2 = middle1+1;

            splitArray(beginning, middle1);
            splitArray(middle2, end);

            merge(beginning, middle1, middle2, end);
        }
    }

    /**
     * Create a working array to combine results into from 2 subarrays
     * Loop through working array and place values into original array
     */
    public void merge(int left, int mid, int mid2, int right) {

        int leftIncr = left;
        int rightIncr = mid2;
        int combinedIncr = left;
        int[] combined = Arrays.copyOf
                (this.collection, collection.length);

        while (leftIncr <= mid && rightIncr <= right) {
            if (collection[leftIncr] <= collection[rightIncr]) {
                combined[combinedIncr++] = collection[leftIncr++];
            } else {
                combined[combinedIncr++] = collection[rightIncr++];
            }
        }

        /**
         * If left or right subarrays are empty
         */
        if (leftIncr >= mid2) {
            while (rightIncr <= right) {
                combined[combinedIncr++] = collection[rightIncr++];
            }
        } else if (rightIncr >= right) {
            while (leftIncr <= mid) {
                combined[combinedIncr++] = collection[leftIncr++];
            }
        }

        /**
         * Copy contents from combined into collection
         */
        for(int i = left; i <= right; i++) {
            collection[i] = combined[i];
        }

        System.out.print("STOP");
    }
}
