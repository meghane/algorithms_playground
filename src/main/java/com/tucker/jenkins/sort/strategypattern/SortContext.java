package com.tucker.jenkins.sort.strategypattern;

/**
 * Created by beezuz on 12/12/15.
 * Strategy Design Pattern for Sorting
 * Use the context to set the specific Sorting Strategy
 */
public class SortContext {
    private SortStrategy strategy;

    public SortContext(SortStrategy strategy) {
        this.strategy = strategy;
    }

    public void sort(int[] collection) {
        strategy.sort(collection);
    }

    public void changeStrategy(SortStrategy strategy) {
        this.strategy = strategy;
    }
}
