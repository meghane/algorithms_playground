package com.tucker.jenkins.sort.strategypattern;

import java.util.Arrays;
import java.util.Collection;

/**
 * Created by beezuz on 12/12/15.
 */
public abstract class SortStrategy {

    public abstract void sort(int[] collection);
}
