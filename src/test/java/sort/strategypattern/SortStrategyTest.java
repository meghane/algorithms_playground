package sort.strategypattern;

import com.tucker.jenkins.sort.MergeSort;
import com.tucker.jenkins.sort.SelectionSort;
import com.tucker.jenkins.sort.strategypattern.SortContext;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.Random;

/**
 * Created by beezus on 10/11/16.
 */
public class SortStrategyTest {

    private int[] toSortRando = new int[100];
    Random random = new Random();
    int max = 75;
    int min = 50;

    private int[] toSortFixed = new int[4];
    private int[] expectedSort = {0,1,2,3};

    SortContext context = new SortContext(new SelectionSort());

    @Before
    public void setupCollection() {
        for (int i = 0; i < toSortRando.length; i++) {
            toSortRando[i] = random.nextInt(max - min) + min;
        }

        for (int i = 0; i < toSortFixed.length; i++) {
            int index = toSortFixed.length - 1;
            toSortFixed[index - i] = i;
        }
    }

    @Test
    public void selectionSortTest() {
        context.sort(toSortFixed);
        context.sort(toSortRando);
        Assert.assertArrayEquals(expectedSort, toSortFixed);
    }

    @Test
    public void mergeSortTest() {
        context.changeStrategy(new MergeSort());
        context.sort(toSortFixed);
        Assert.assertArrayEquals(expectedSort, toSortFixed);
    }

}
